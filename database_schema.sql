--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6rc1
-- Dumped by pg_dump version 10.0

-- Started on 2017-12-02 00:13:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2193 (class 1262 OID 37160)
-- Name: fimleikaskipan; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE fimleikaskipan WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Faroese_Faroe Islands.1252' LC_CTYPE = 'Faroese_Faroe Islands.1252';


\connect fimleikaskipan

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2195 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 193 (class 1259 OID 37202)
-- Name: competitions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE competitions (
    id bigint NOT NULL,
    name text,
    start_date date
);


--
-- TOC entry 192 (class 1259 OID 37193)
-- Name: events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE events (
    id bigint NOT NULL,
    name text,
    competition bigint NOT NULL
);


--
-- TOC entry 191 (class 1259 OID 37191)
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2196 (class 0 OID 0)
-- Dependencies: 191
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- TOC entry 188 (class 1259 OID 37171)
-- Name: participants; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE participants (
    id bigint NOT NULL,
    name text,
    date_of_birth date
);


--
-- TOC entry 187 (class 1259 OID 37169)
-- Name: participants_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2197 (class 0 OID 0)
-- Dependencies: 187
-- Name: participants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE participants_id_seq OWNED BY participants.id;


--
-- TOC entry 186 (class 1259 OID 37163)
-- Name: scores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE scores (
    id bigint NOT NULL,
    participant bigint NOT NULL,
    team bigint NOT NULL,
    event bigint NOT NULL,
    e double precision,
    d double precision,
    total double precision
);


--
-- TOC entry 185 (class 1259 OID 37161)
-- Name: scores_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE scores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2198 (class 0 OID 0)
-- Dependencies: 185
-- Name: scores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE scores_id_seq OWNED BY scores.id;


--
-- TOC entry 197 (class 1259 OID 37247)
-- Name: sessions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sessions (
    id bigint NOT NULL,
    user_id bigint NOT NULL
);


--
-- TOC entry 196 (class 1259 OID 37245)
-- Name: sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2199 (class 0 OID 0)
-- Dependencies: 196
-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sessions_id_seq OWNED BY sessions.id;


--
-- TOC entry 190 (class 1259 OID 37182)
-- Name: teams; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE teams (
    id bigint NOT NULL,
    name text NOT NULL
);


--
-- TOC entry 189 (class 1259 OID 37180)
-- Name: teams_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2200 (class 0 OID 0)
-- Dependencies: 189
-- Name: teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE teams_id_seq OWNED BY teams.id;


--
-- TOC entry 195 (class 1259 OID 37236)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id bigint NOT NULL,
    username text NOT NULL,
    salt_hash text NOT NULL
);


--
-- TOC entry 194 (class 1259 OID 37234)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2201 (class 0 OID 0)
-- Dependencies: 194
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 2043 (class 2604 OID 37196)
-- Name: events id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- TOC entry 2041 (class 2604 OID 37174)
-- Name: participants id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY participants ALTER COLUMN id SET DEFAULT nextval('participants_id_seq'::regclass);


--
-- TOC entry 2040 (class 2604 OID 37166)
-- Name: scores id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY scores ALTER COLUMN id SET DEFAULT nextval('scores_id_seq'::regclass);


--
-- TOC entry 2045 (class 2604 OID 37250)
-- Name: sessions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sessions ALTER COLUMN id SET DEFAULT nextval('sessions_id_seq'::regclass);


--
-- TOC entry 2042 (class 2604 OID 37185)
-- Name: teams id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY teams ALTER COLUMN id SET DEFAULT nextval('teams_id_seq'::regclass);


--
-- TOC entry 2044 (class 2604 OID 37239)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2059 (class 2606 OID 37209)
-- Name: competitions competitions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY competitions
    ADD CONSTRAINT competitions_pkey PRIMARY KEY (id);


--
-- TOC entry 2056 (class 2606 OID 37201)
-- Name: events events_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- TOC entry 2052 (class 2606 OID 37179)
-- Name: participants participants_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participants
    ADD CONSTRAINT participants_pkey PRIMARY KEY (id);


--
-- TOC entry 2050 (class 2606 OID 37168)
-- Name: scores scores_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scores
    ADD CONSTRAINT scores_pkey PRIMARY KEY (id);


--
-- TOC entry 2066 (class 2606 OID 37252)
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- TOC entry 2054 (class 2606 OID 37190)
-- Name: teams teams_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT teams_pkey PRIMARY KEY (id);


--
-- TOC entry 2061 (class 2606 OID 37260)
-- Name: users username_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT username_unique UNIQUE (username);


--
-- TOC entry 2063 (class 2606 OID 37244)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2057 (class 1259 OID 37215)
-- Name: fki_competition_fk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_competition_fk ON events USING btree (competition);


--
-- TOC entry 2046 (class 1259 OID 37233)
-- Name: fki_event_fk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_event_fk ON scores USING btree (event);


--
-- TOC entry 2047 (class 1259 OID 37221)
-- Name: fki_participant_fk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_participant_fk ON scores USING btree (participant);


--
-- TOC entry 2048 (class 1259 OID 37227)
-- Name: fki_team_fk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_team_fk ON scores USING btree (team);


--
-- TOC entry 2064 (class 1259 OID 37258)
-- Name: fki_users_fk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_users_fk ON sessions USING btree (user_id);


--
-- TOC entry 2070 (class 2606 OID 37210)
-- Name: events competition_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events
    ADD CONSTRAINT competition_fk FOREIGN KEY (competition) REFERENCES competitions(id);


--
-- TOC entry 2069 (class 2606 OID 37228)
-- Name: scores event_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scores
    ADD CONSTRAINT event_fk FOREIGN KEY (event) REFERENCES events(id);


--
-- TOC entry 2067 (class 2606 OID 37216)
-- Name: scores participant_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scores
    ADD CONSTRAINT participant_fk FOREIGN KEY (participant) REFERENCES participants(id);


--
-- TOC entry 2068 (class 2606 OID 37222)
-- Name: scores team_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY scores
    ADD CONSTRAINT team_fk FOREIGN KEY (team) REFERENCES teams(id);


--
-- TOC entry 2071 (class 2606 OID 37253)
-- Name: sessions users_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT users_fk FOREIGN KEY (user_id) REFERENCES users(id);


-- Completed on 2017-12-02 00:13:11

--
-- PostgreSQL database dump complete
--

