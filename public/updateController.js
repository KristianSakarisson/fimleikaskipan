$(document).ready(function() {
  var table = $('#updateTable').DataTable({
    ordering: false,
    searching: false,
    paging: false,
    info: false
  })

  $('#addRow').on('click', function() {
    var newRow = table.row.add([
      '<input id="name" name="name" placeholder="Navn" type="text">', 
      '<input id="team" name="team" placeholder="Felag" type="text">', 
      '<input id="e" name="e" placeholder="E" type="text">', 
      '<input id="d" name="d" placeholder="D" type="text">', 
      '<input id="total" name="total" placeholder="Total" type="text">'
    ]).draw( false )
    
    newRow.id = 'hello'
    newRow.draw()
    console.log(newRow)
})

  $('#removeAll').on('click', function() {
    table.clear()
    table.draw()
    $('#addRow').click()
  })

  $('#updateTable tbody').on('click', 'tr', function() {
    if($(this).hasClass('selected')) {
      $(this).removeClass('selected')
    } else {
      table.$('tr.selected').removeClass('selected')
      $(this).addClass('selected')
    }
  })

  $('#removeRow').on('click', function() {
    table.row('.selected').remove().draw(false)
  })
})