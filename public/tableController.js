$(document).ready(function() {
  var table = $('#test').DataTable({
    searching: false,
    info: false,
    order: [[4, 'desc']],
    pageLength: 8,
    bLengthChange: false //used to hide the property  
  })

  var socket = io.connect('/')
  // socket.on('newResults', function(results) {
  //   console.log('new results!')
  //   table.clear()
  //   table.draw()
  //   results.data.forEach(function(dataObject) {
  //     table.row.add([
  //       dataObject.name, 
  //       dataObject.team, 
  //       dataObject.e, 
  //       dataObject.d, 
  //       dataObject.total
  //     ]).draw()
  //   })
  //   $('#heading').html(results.dicipline)
  // })
  socket.on('newResults', function(results) { 
    location.reload()
  })

  var cyclePages = setInterval(function() {
    console.log(table.page() * table.page.len())
    console.log(table.rows().count())
    if((table.page() + 1) * table.page.len() < table.rows().count()) {
      table.page('next').draw('page')
    } else {
      table.page('first').draw('page')
    }
  }, 30000)
})
