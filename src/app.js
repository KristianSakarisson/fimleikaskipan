/**
 * A super simple web application that shows scores of gymnastics competitions
 * Written by Kristian Sakarisson for Fimleikasamband Føroya
 * All queries must be prepared!
 * For questions, contact kristian@sakarisson.com
 */

require('dotenv').config()
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const path = require('path')

global.__basedir = path.join(__dirname, '../')
app.set('view engine', 'pug')
app.set('views', path.join(global.__basedir, '/views'))
app.locals.basedir = path.join(global.__basedir, '/views')
app.use('/public', express.static('public'))
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(require('./middleware').flashMessager)
app.use(require('./middleware').validateSession)
app.use(require('./middleware').userView)
require('./routes')(app)

module.exports = app

// app.listen(12345, () => console.log('Express server started'))
