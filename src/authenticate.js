const client = require('./connection')
const jwt = require('jsonwebtoken')
const async = require('async')
const bcrypt = require('bcrypt')

module.exports.deactivateOldTokens = (id) => new Promise((resolve, reject) => {
  client.query('delete from sessions where user_id = $1', [id], (err, data) => {
    if (err) reject(err)
    else resolve()
  })
})

module.exports.generateNewToken = (username) => new Promise(async (resolve, reject) => {

  const getUserId = (username) => new Promise((resolve, reject) => {
    client.query('select id from users where username = $1', [username], (err, res) => {
      if (err) reject(err)
      else resolve(res.rows[0].id)
    })
  })

  const addNewSessionToken = (id) => new Promise((resolve, reject) => {
    client.query('insert into sessions (user_id) values ($1) returning *', [id], (err, res) => {
      if (err) reject(err)
      else resolve(res.rows[0].id)
    })
  })

  const generateToken = (data) => new Promise((resolve, reject) => {
    console.log(data)
    jwt.sign({
      username: data.username,
      userId: data.userId,
      tokenId: data.tokenId
    }, process.env.JWT_SECRET, {
      expiresIn: '2d'
    }, (err, token) => {
      if (err) reject(err)
      else resolve(token)
    })
  })

  try {
    const id = await getUserId(username)
    await this.deactivateOldTokens(id)
    const sessionId = await addNewSessionToken(id)
    const token = await generateToken({
      username: username,
      userId: id,
      tokenId: sessionId
    })
    resolve(token)
  } catch (e) {
    console.log(e)
    reject(e)
  }
})

module.exports.validateLogin = (username, password) => new Promise((resolve, reject) => {
  client.query('select * from users where username = $1', [username], async (err, data) => { // Get user
    if (err) {
      return reject(err)
    }
    if (data.rows < 1) {
      const err = new Error('Invalid username')
      return reject(err)
    }
    try {
      const validPassword = await bcrypt.compare(password, data.rows[0].salt_hash)
      if (validPassword) resolve(await this.generateNewToken(username))
      else reject (new Error('Wrong password'))
    } catch (e) {
      reject(e)
    }
  })
})

module.exports.createUser = (username, name, password) => new Promise((resolve, reject) => {
  const salt_hash = bcrypt.hashSync(password, 10)
  client.query('insert into users (username, real_name, salt_hash) values ($1, $2, $3)', [username, name, salt_hash], (err, res) => {
    if (err) reject(err)
    else resolve()
  })
})

/**
 * Validate admin. Middleware to make sure that requester is a logged in admin.
 * Renders 401 page if validation fails. Otherwise allows access
 */
module.exports.validateAdmin = (req, res, next) => {
  if (!req.validSession) {
    res.locals.messages.push({ type: 'danger', text: 'Tú hevur ikki atgond til hesa síðu'} )
    res.status(401).render('errors/401')
  } else {
    next()
  }
}
