const jwt = require('jsonwebtoken')
const client = require('./connection')

module.exports.flashMessager = (req, res, next) => {
  res.locals.messages = []
  next()
}

// Validate JSON-token of current user
module.exports.validateSession = async (req, res, next) => {
  const token = req.cookies.token
  req.validSession = false // Session is invalid by default

  const decodeToken = (token) => new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT_SECRET, (error, decoded) => {
      if (error) { // If token is invalid, clear it from user's cache
        reject(error)
      } else { // Otherwise, tell future middleware that session is valid
        req.validSession = true
        req.sessionDetails = decoded
        resolve(decoded)
      }
    })
  })

  const checkSessionDatabase = (decodedToken) => new Promise((resolve, reject) => {
    client.query('select * from sessions where id = $1', [decodedToken.tokenId], (err, res) => {
      if (err) {
        reject(err)
      } else if (res.rows.length === 0) {
        const err = new Error('Session not in database')
        reject(err)
      } else {
        resolve(true)
      }
    })
  })

  try {
    const decodedToken = await decodeToken(token)
    const validSession = await checkSessionDatabase(decodedToken)
    req.validSession = validSession
    req.sessionDetails = decodedToken
  } catch (e) {
    if (token) res.locals.messages.push({ type: 'danger', text: 'Tú bleiv sjálvvirkandi útritaður av tí at tín sessión gekk út' })
    res.clearCookie('token')
  }
  next()
}

// Add session details to locals if user is logged in
module.exports.userView = async (req, res, next) => {
  const getCompetitions = () => new Promise((resolve, reject) => {
    client.query('select * from competitions', (err, data) => {
      if (err) reject(err)
      else resolve(data.rows)
    })
  })
  if (req.validSession) {
    res.locals.sessionDetails = req.sessionDetails
  }
  res.locals.competitions = await getCompetitions()
  next()
}
