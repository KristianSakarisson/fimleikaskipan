const app = require('./app')

const port = process.env.PORT || 80

app.listen(port, (err) => {
  if (err) {
    console.error('An error occurred starting the server: ' + err.message());
  } else {
    console.log(`Server is listening on port ${port}`)
  }
})
