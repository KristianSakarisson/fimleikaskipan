const authenticate = require('./authenticate')
const f = require('./functions')

module.exports = (app) => {
  app.get('/', (req, res, next) => {
    res.render('index')
  })

  app.get('/kappingar/:name', async (req, res, next) => {
    try {
      const competitionDetails = await f.getCompetitionDetails(req.params.name, 2017)
      res.render('competition', {
        details: competitionDetails
      })
    } catch (e) {
      res.locals.messages.push({ type: 'danger', text: 'Kapping bleiv ikki funnin' })
      next()
    }
  })

  app.get('/login', (req, res, next) => {
    res.render('login')
  })

  app.post('/login', async (req, res, next) => {
    try {
      const token = await authenticate.validateLogin(req.body.username, req.body.password) // Create JSON token
      res.cookie('token', token) // Set token
      res.locals.messages.push({ type: 'success', text: 'Tú ert innritaður' })
      res.render('login')
    } catch (err) {
      res.locals.messages.push({ type: 'warning', text: 'Skeivt brúkaranavn ella loyniorð' })
      res.render('login')
    }
  })

  /**
   * Logout function. If user logs out, their session should be wiped from database
   */
  app.get('/logout', async (req, res, next) => {
    if (req.validSession) {
      try {
        await authenticate.deactivateOldTokens(res, locals.sessionDetails.userId)
      } catch (e) {
        console.log('Error in deactivating old tokens. User:')
        console.log(res.locals.sessionDetails)
        console.log(e)
      }
      res.clearCookie('token')
      res.locals.messages.push({ type: 'success', text: 'Tú ert útritaður' })
      res.render('login')
    } else {
      res.locals.messages.push({ type: 'danger', text: 'Tað bar ikki til at rita út' })
      res.render('login')
    }
  })

  app.get('/admin', authenticate.validateAdmin, (req, res, next) => {
    res.render('adminpanel')
  })

  app.get('/admin/users', authenticate.validateAdmin, async (req, res, next) => {
    try {
      const users = await f.getUsers()
      res.render('admin/users', {
        users: users
      })
    } catch (e) {
      res.locals.messages.push({ type: 'danger', text: e })
      res.render('errors/500')
    }
  })

  app.delete('/admin/users', authenticate.validateAdmin, (req, res, next) => {
    console.log('delete!')
    res.render('admin/users')
  })

  app.get('/createUser', authenticate.validateAdmin, (req, res, next) => {
    res.render('admin/createUser')
  })

  app.post('/createUser', authenticate.validateAdmin, async (req, res, next) => {
    const username = req.body.username
    const name = req.body.name
    const password = req.body.password
    try {
      await authenticate.createUser(username, name, password)
      res.locals.messages.push({ type: 'success', text: `Brúkarin ${username} bleiv stovnaður`})
    }
    catch (e) {
      res.locals.messages.push({ type: 'danger', text: 'Ein feilur hendi:\n' + e})
    }
    res.render('admin/createUser')
  })

  app.get('/createParticipant', authenticate.validateAdmin, async (req, res, next) => {
    const participants = await f.getParticipants()
    res.render('admin/participants', {
      participants: participants
    })
  })

  app.use((req, res, next) => {
    res.status(404).render('errors/404')
  })
}
