const client = require('./connection')

module.exports.getUsers = () => new Promise((resolve, reject) => {
  client.query('select * from users', (err, data) => {
    if (err) reject(err)
    else resolve(data.rows)
  })
})

module.exports.getParticipants = () => new Promise((resolve, reject) => {
  client.query('select * from participants', (err, data) => {
    if (err) reject(err)
    else resolve(data.rows)
  })
})

module.exports.getCompetitionDetails = (name, year) => new Promise(async (resolve, reject) => {
  const getCompetition = (name, year) => new Promise((resolve, reject) => {
    client.query('select * from competitions where name = $1 and extract(year from start_date) = $2', [name, year], (err, data) => {
      if (err || data.rows.length === 0) reject()
      else resolve(data.rows[0])
    })
  })

  const getEvents = (competitionId) => new Promise((resolve, reject) => {
    client.query('select * from events where competition = $1', [competitionId], (err, data) => {
      if (err) reject()
      else resolve(data.rows)
    })
  })

  const getScores = (eventId) => new Promise((resolve, reject) => {
    client.query(`
      select events.name as event_name, participants.name as participant_name, participants.date_of_birth, teams.name as team_name, e, d, total from scores, participants, teams, events
      where event = $1
      and scores.participant = participants.id
      and scores.team = teams.id
      and scores.event = events.id`, [eventId]
    , (err, data) => {
      if (err) reject(err)
      else resolve(data.rows)
    })
  })

  try {
    const competition = await getCompetition(name, year)
    const events = await getEvents(competition.id)
    const scores = await Promise.all(events.map((event) => getScores(event.id)))
    console.log(scores)
    resolve({ comp: competition, scores: scores })
  } catch (e) {
    reject(e)
  }
})
