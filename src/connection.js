const { Client } = require('pg')
const db = new Client({
  user: process.env.DBUSER,
  database: process.env.DATABASE,
  host: 'localhost',
  password: process.env.DBPASSWORD,
  port: 5432,
  idleTimeoutMillis: 3000
})

db.connect((err) => err ? console.log(err) : console.log('Database client connected'))

module.exports = db
